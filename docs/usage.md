# Usage of pac-mule

This document describes commands, flags, and environment variables for pac-mule.

## Flags

```
--log-level string   Specify log level for output. (default "info")
--access-log         Specify weather to run with or without HTTP access logs. (default true)
--port string        Starting server port. (default "8080")
--pac-file string    Location of PAC file. (default "/etc/pac-mule/config.pac")
--pid-file string    Specify server PID file path. (default "/var/run/pac-mule.pid")
--tls                Specify weather to run server in secure mode.
--tls-cert string    Specify TLS certificate file path.
--tls-key string     Specify TLS key file path.
-h, --help               Show this help
--version            Display version information
```

## Environment Variables

```
MULE_LOG_LEVEL      Specify log level for output. (default "info")
MULE_ACCESS_LOG     Specify weather to run with or without HTTP access logs. (default true)
MULE_SERVER_PORT    Starting server port. (default "8080")
MULE_PAC_FILE       Location of PAC file. (default "/etc/pac-mule/config.pac")
MULE_SERVER_PID     Specify server PID file path. (default "/var/run/pac-mule.pid")
MULE_TLS            Specify weather to run server in secure mode.
MULE_TLS_CERT       Specify TLS certificate file path.
MULE_TLS_KEY        Specify TLS key file path.
```