# Download and build

## System requirements
For development it's best to develop this software on either Linux or MacOS machines as this is targeted towards POSIX compliant machines.

For production you will need:

* POSIX Compliant Operating System (Linux, BSD, MacOS, etc.).
* Network connection to other computers on your network.
* At least 10MiB of hard drive space.

## Download pre-built binary from releases

You can grab a pre compiled binary in the [releases](https://gitlab.com/junland/pac-mule/releases) section of this repository which is is the fastest way to get this software.

## Building binary from source

### Prerequisites

`go` - 1.8.x or higher, with `GOPATH` set up.

`make` - For using the `Makefile`.

Once your prerequisites have been installed follow these steps:

1. `git clone https://github.com/junland/pac-mule.git`

2. `cd pac-mule`

3. `make binary`

These steps should produce a binary called `pac-mule` which you can execute from your current shell.

## Verifying the binary

To verify that your downloaded or built binary executes correct is to issue this command:

```
./pac-mule --version
```

This should display version text of the compiled binary.
