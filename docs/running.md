## Running pac-mule

To start a basic instance of pac-mule you can fill in the needed parameters and run this command:

```
./pac-mule --pac-file=./config.pac
```

Now clients can get the config from `http://localhost:8080/config`

You can also launch a more secure instance of pac-mule by generating a SSL certificate and key. Doing this you can run this command:

```
./pac-mule --tls --tls-cert=./cert --tls-key=./key --pac-file=./config.pac
```

Now clients can get the config from `https://localhost/config`