package server

import (
	"net/http"
)

// helloRootHandle is a handle.
func handleRoot(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/config", 301)
	return
}

// helloGlobalHandle is a example handler.
func (f PACFile) handlePACFile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-ns-proxy-autoconfig")
	w.Write([]byte(f.content))
	return
}
