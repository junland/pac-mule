package server

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"

	log "github.com/sirupsen/logrus"
)

// Config struct provides configuration fields for the server.
type Config struct {
	LogLvl string
	Access bool
	Port   string
	PAC    string
	PID    string
	TLS    bool
	Cert   string
	Key    string
}

// PACFile describes a proxy auto configuration file.
type PACFile struct {
	content string
}

var stop = make(chan os.Signal)

// Start sets up and starts the main server application
func Start(c Config) error {
	log.Debug("Setting up logging...")

	// Get log level environment variable.
	envLvl, err := log.ParseLevel(c.LogLvl)
	if err != nil {
		fmt.Println("Invalid log level ", envLvl)
	} else {
		// Setup logging with Logrus.
		log.SetLevel(envLvl)
	}

	// This can be adjusted, however just defaulted to 1MiB for the file.
	stat, err := os.Stat(c.PAC)
	if err != nil {
		log.Fatal(err)
	}

	if stat.Size() > int64(1048576) {
		log.Fatal("PAC file is bigger than 1 MiB")
	}

	log.Info("PAC file size is OK...")

	b, err := ioutil.ReadFile(c.PAC)
	if err != nil {
		fmt.Print(err)
	}

	content := string(b)

	if c.TLS == true {
		if c.Cert == "" || c.Key == "" {
			log.Fatal("Invalid TLS configuration, please pass a file path for both MULE_KEY and MULE_CERT")
		}
	}

	log.Info("Setting up server...")

	router := RegisterRoutes(content)

	srv := &http.Server{Addr: ":" + c.Port, Handler: AccessLogger(router, c.Access)}

	log.Debug("Starting server on port ", c.Port)

	go func() {
		if c.TLS == true {
			err := srv.ListenAndServeTLS(c.Cert, c.Key)
			if err != nil {
				log.Fatal("ListenAndServeTLS: ", err)
			}
		}
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()

	log.Info("Serving on port " + c.Port + ", press CTRL + C to shutdown.")

	p := CreatePID(c.PID)

	signal.Notify(stop, os.Interrupt)

	log.Warn("After notify...")

	<-stop // wait for SIGINT

	log.Warn("Shutting down server...")

	p.RemovePID()

	ctx, cancel := context.WithTimeout(context.Background(), 45*time.Second) // shut down gracefully, but wait no longer than 45 seconds before halting.

	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}

	return nil
}
