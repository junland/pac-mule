# pac-mule 
[![Build Status](https://travis-ci.org/junland/pac-mule.svg?branch=master)](https://travis-ci.org/junland/pac-mule)

Simple server written in Go that issues a proxy auto configuration (or [PAC](https://en.wikipedia.org/wiki/Proxy_auto-config)) to clients.

## Getting pac-mule

The easiest way to get `pac-mule` is to grab a pre-compiled binary in the [releases](https://gitlab.com/junland/pac-mule/releases) section of this repository. Directions on running this software are [here][running]

If you want build from source please follow [this][download_build] documentation. 

## Documentation

If you would like to know more about this software, you can visit the repository documentation which is located [here][docs].

## Built With

`github.com/justinas/alice` - Simple middleware chaining library.

`github.com/sirupsen/logrus` -  Structured, pluggable logging for Go.

`github.com/spf13/pflag` - Drop in replacement for the `flag` package.

`github.com/julienschmidt/httprouter` - A high performance HTTP request router that scales well.

## Versioning

I use [SemVer 2.0.0](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/junland/pak-mule/tags).

## Authors

* **John Unland** - *Initial work* - [junland](https://gitlab.com/junland)

See also the list of [contributors](https://gitlab.com/your/project/contributors) who participated in this project

## License

This project is licensed under the GPLv2 License - see the [LICENSE](LICENSE.md) file for details.

[docs]: docs
