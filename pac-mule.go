// +build !windows

// Package main is a simple wrapper of the real pac-mule entrypoint package.
//
// This package should NOT be extended or modified in any way; to modify the
// pac-mule binary, work in the `gitlab.com/junland/pac-mule/cmd` package.
//
package main

import (
	mule "gitlab.com/junland/pac-mule/cmd"
)

func main() {
	mule.Run()
}
